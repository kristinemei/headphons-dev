// Global var to hold current form qty values.
var order_qty = [];
Shopify.queue = [];

Shopify.moveAlong = function() {
  // If we still have requests in the queue, let's process the next one.
  if ( Shopify.queue.length ) {
    var request = Shopify.queue.shift();
    Shopify.addItem(request.variantId, request.quantity, request.properties, Shopify.moveAlong);
  } else {
     document.location.href = '/cart';
    }
};

Shopify.addItem = function(id, qty, properties, callback) {
  if ( id == undefined ) {
    return;
  }

  var params = {
    quantity: qty,
    id: id
  };

  if(properties != false){
    params.properties = properties;
  }
  $.ajax({
    type: "POST",
    url: "/cart/add.js",
    data: params,
    dataType: "json"
  })
  .done(function(t) {
    "function" == typeof e ? e(t) : Shopify.onItemAdded(t)
  })
  .always(function(t) {
    Shopify.moveAlong();
  })
  .fail(function(t, r) {
    alert( "error" );
    Shopify.onError(t, r);
  });

}

Shopify.onItemAdded = function(t) {
 console.log(t.title + " was added to your shopping cart.")
}

//Fires when add to cart clicked.
$(".bulkAddToCart").on("click", function( event ) {

  //ajax loading graphic...
document.getElementById("loading").style.display = "block";

//Loop through tablr rows or other elements.
$("#product-upsell-form .product-upsell-grid .item").each(function( index ) {

  //get variant ID
  var variant_id = $("#variant-id" + index).val();
  // get qty
  var quantity = 1;

  //Was there a change in QTY? If no change then do not push.
  //This saves unnecessary calls and speeds up execution.
  if (order_qty[index] == quantity) {
    return;
  }

  //Get line item properties if there are any.
  //Can be commented out if line props are not required.


  var prop_amount = $("#amount" + index).val();
  var prop_json = {"amount": prop_amount};
    Shopify.queue.push({
        variantId: variant_id,
        quantity: parseInt(quantity, 10) || 0,
        properties: prop_json
    });
  });

  Shopify.moveAlong();

});

//On Document Load store the current qty values for each product
//Only required for bulk operations.
window.addEventListener('DOMContentLoaded', function() {
  order_qty = [];
  $("#product-upsell-form .product-upsell-grid .item").each(function( index ) {
    order_qty.push($('#quantity' + index).val());
  });
});
